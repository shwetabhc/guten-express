<?php return array(
    'root' => array(
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '4aa39076eeaef7471da5901ac343c4f52bc86227',
        'name' => 'greyhatch/guten-express',
        'dev' => true,
    ),
    'versions' => array(
        'greyhatch/guten-express' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '4aa39076eeaef7471da5901ac343c4f52bc86227',
            'dev_requirement' => false,
        ),
    ),
);
