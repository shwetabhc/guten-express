=== Guten Express ===
Contributors:      greyhatch
Donate link:       https://www.greyhatch.com/
Tags:              block, gutenberg, editor, custom, carousel, slider, featured, posts, flip, card
Requires PHP:      7.1
Requires at least: 5.5
Tested up to:      5.8
Stable tag:        0.1.0
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html

Guten Express - Powerful, Feature Rich Gutenberg Blocks.

== Description ==
Guten Express is a WordPress plugin for Gutenberg editor. This plugin adds new feature rich blocks in WordPress Gutenberg editor like Customizable Carousels, Featured Posts and Flip Cards.



== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/guten-express` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress


== Frequently Asked Questions ==


= Is the plugin free to use? =

Yes the plugin is totally free.

= Do I need to install Gutenberg plugin to use Guten Express? =

Yes, Since Guten Express is a plugin that adds blocks to the Gutenberg editor, Gutenberg is required for Guten Express to work properly. 

== Screenshots ==
 
1. screenshot-1.gif
2. screenshot-2.gif
3. screenshot-3.gif

== Changelog ==

= 0.1.0 =
* Release


