/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';
import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import defaultImg from './assets/Default.png';
import overlayImg from './assets/Overlay.png';
import alternateImg from './assets/Alternate.png';
import featuredPostPreview from './assets/featuredPostsPreview.gif';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import { useBlockProps, RichText, InspectorControls } from '@wordpress/block-editor';
import { PanelBody } from '@wordpress/components';


/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({ attributes, setAttributes }) {
	const { selectedPosts, gridSize, gridClass, stylingOption, excerptToggle, blockTitle, preview } = attributes;
	const featuredMedia = [...wpConstants.allThumbnails];
	const [dropdownList, setDropdownList] = useState([]);
	const [posts, setPosts] = useState([]);
	const [gridSizeState, setGridSizeState] = useState({ value: 3, selected: false });
	const [selectedPostsState, setSelectedPostsState] = useState([]);
	const [stylingOptionState, setStylingOptionState] = useState('default');
	const [excerptToggleState, setExcerptToggleState] = useState(true);
	useEffect(() => {
		fetch(wpConstants.siteUrl + '/wp-json/wp/v2/posts')
			.then(res => res.json())
			.then(data => {
				setPosts([...data]);
				console.log(data);
				console.log(wpConstants.allPosts);
				console.log(wpConstants.allThumbnails)
				// Populating the List of Posts
				let tempList = [];
				data.map(d => {
					tempList.push({ label: d.title.rendered, value: d.id });
				})
				setDropdownList([...tempList]);
				setStylingOptionState(stylingOption);
				setExcerptToggleState(excerptToggle);

				setAttributes({ excerptToggle: excerptToggle });

				// Checking for Changes in Featured Media
				selectedPosts.map(post => {
					data.map(d => {
						if (post.postId === d.id) {
							console.log(d.featured_media);
							post.postImage = d.featured_media;
						}
					})
				})
				setSelectedPostsState([...selectedPosts]);
				// Setting Grid size if it is available.
				if (parseInt(gridSize)) {
					setGridSizeState({
						value: parseInt(gridSize),
						class: gridClass,
						selected: true
					})
				}

			});
	}, [])

	const onGridSelect = (e) => {
		let selectedValue = 0;
		let gridClass = '';
		if (e.target.value === "4 x 1") {
			selectedValue = 4;
			gridClass = 'grid-fourbyone';
		} else if (e.target.value === "1 x 4") {
			selectedValue = 4;
			gridClass = 'grid-one';
		} else if (e.target.value === "2 x 2") {
			selectedValue = 4;
			gridClass = 'grid-twobytwo';
		} else if (e.target.value === "2 x 1") {
			selectedValue = 2;
			gridClass = 'grid-twobyone';
		} else if (e.target.value === "1 x 2") {
			selectedValue = 2;
			gridClass = 'grid-one';
		}
		else if (e.target.value === "3 x 1") {
			selectedValue = 3;
			gridClass = 'grid-threebyone';
		} else if (e.target.value === "1 x 3") {
			selectedValue = 3;
			gridClass = 'grid-one';

		}
		setGridSizeState({
			class: gridClass,
			value: selectedValue,
			selected: true
		})
		setAttributes({ gridClass: gridClass });
		setAttributes({ gridSize: selectedValue });
	}

	const RenderDropdown = (props) => {
		let dropdowns = [];
		const [selectedOptions, setSelectedOptions] = useState([
			{ label: '', value: '' },
			{ label: '', value: '' },
			{ label: '', value: '' },
			{ label: '', value: '' }]);
		useEffect(() => {
			selectedPostsState.map((post, index) => {
				const tempSelected = posts.filter(p => parseInt(p.id) === parseInt(post.postId))[0];
				const label = tempSelected ? tempSelected.title.rendered : null;
				selectedOptions[index] = { label: label, value: post.postId };
			})
			setSelectedOptions([...selectedOptions]);
		}, [])
		for (let i = 0; i < props.grids; i++) {
			dropdowns.push(
				<div>
					<p><strong>Select Post {i + 1} :</strong></p>

					<Select value={selectedOptions[i]} options={dropdownList} onChange={(selectedValue) => {

						const selectedPost = posts.filter(post => parseInt(post.id) === parseInt(selectedValue.value))[0];
						let tempSelected = [...selectedPosts];
						tempSelected[i] = {
							postId: selectedPost.id,
							postImage: (selectedPost.featured_media)
						};
						setAttributes({ selectedPosts: tempSelected });
						setSelectedPostsState([...tempSelected]);
					}} />
				</div>

			)
		}
		return dropdowns;
	}

	const RenderGrids = (props) => {
		let gridElements = [];
		for (let i = 0; i < props.grids; i++) {
			// filtering data to display
			const headerContainer = (featuredMedia.filter(media => media.id === selectedPostsState[i].postImage)[0]) ? stylingOptionState : "";
			console.log(selectedPostsState[i].postImage);
			if (featuredMedia) {
				console.log(featuredMedia);
			}
			const headerElement = posts.filter(post => post.id === selectedPostsState[i].postId)[0];
			const headerText = (headerElement ? headerElement.title.rendered : null)

			const imageElement = featuredMedia.filter(media => media.id === selectedPostsState[i].postImage)[0];
			const imageSource = imageElement ? imageElement.source_url : null

			const excerptElement = posts.filter(post => post.id === selectedPostsState[i].postId)[0];
			const excerptContent = excerptElement ? excerptElement.excerpt.rendered : null;

			if (selectedPostsState[i].postId !== null) {
				gridElements.push(
					<div key={i} className="featured-posts-grid">
						<div className={headerContainer} >
							<div className="overlay"></div>
							<h3 dangerouslySetInnerHTML={{ __html: headerText }}></h3>
							<img src={imageSource} />
						</div>
						{excerptToggle && <div dangerouslySetInnerHTML={{ __html: excerptContent }}></div>}
					</div>
				)
			}
		}
		return gridElements;
	}

	if (preview === true) {
		return <>
			<img src={wpConstants.pluginUrl + featuredPostPreview} />
		</>
	}


	return (
		<p {...useBlockProps()}>
			<InspectorControls>
				<PanelBody title={'Grid Size'} >
					<p><strong>Select a Grid Size:</strong></p>
					<select onChange={onGridSelect}>
						<option value="0" >Grid Size</option>
						<option value="1 x 2" selected={gridClass == "grid-one"}>1 x 2</option>
						<option value="1 x 3" selected={gridClass == "grid-one"}>1 x 3</option>
						<option value="1 x 4" selected={gridClass == "grid-one"}>1 x 4</option>
						<option value="2 x 1" selected={gridClass == "grid-twobyone"}>2 x 1</option>
						<option value="2 x 2" selected={gridClass == "grid-twobytwo"}>2 x 2</option>
						<option value="3 x 1" selected={gridClass == "grid-threebyone"}>3 x 1</option>
						<option value="4 x 1" selected={gridClass == "grid-fourbyone"}>4 x 1</option>

					</select>
				</PanelBody>
				<PanelBody title={'Search Posts'} >
					{gridSizeState.selected ? <RenderDropdown grids={gridSizeState.value} /> : null}

				</PanelBody>
				<PanelBody title={'Styling'} >
					<p><strong>Display Options </strong></p>
					<form className="guten-express-header-style-selector" onChange={(e) => {
						setStylingOptionState(e.target.value);
						setAttributes({ stylingOption: e.target.value });
						console.log(e.target.value);

					}}>

						<label>
							<input
								name="display-options"
								type="radio"
								value="featured-posts-header-default"
								checked={stylingOptionState === 'featured-posts-header-default'}
							/>
							<img src={wpConstants.pluginUrl + defaultImg} />
						</label>



						<label>
							<input
								name="display-options"
								type="radio"
								value="featured-posts-header-with-overlay"
								checked={stylingOptionState === 'featured-posts-header-with-overlay'}
							/>
							<img src={wpConstants.pluginUrl + overlayImg} />
						</label>



						<label>
							<input
								name="display-options"
								type="radio"
								value="featured-posts-header-alternate"
								checked={stylingOptionState === 'featured-posts-header-alternate'}
							/>
							<img src={wpConstants.pluginUrl + alternateImg} />
						</label>


					</form>
					<p><strong>Display Excerpt:</strong></p>
					<input
						type="checkbox"
						checked={excerptToggleState}
						value={excerptToggleState}
						onChange={(e) => {
							setExcerptToggleState(!excerptToggleState);
							setAttributes({ excerptToggle: !excerptToggleState });
						}} />

				</PanelBody>
			</InspectorControls>
			<div className="featured-posts-grid-top">
				<RichText placeholder="Title" tagName="h2" value={blockTitle} onChange={(value) => { setAttributes({ blockTitle: value }) }} />
			</div>
			<div className={"featured-posts-container " + gridSizeState.class}>
				{gridSizeState.selected ?
					<RenderGrids grids={gridSizeState.value} /> : null}
			</div>
		</p>
	);
}
