import { registerBlockType } from '@wordpress/blocks';
import './style.scss';
import FeaturedPostsEdit from './edit';
import FeaturedPostsSave from './save';

const register = () => {

    registerBlockType('guten-express/featured-posts', {
        attributes: {
            blockTitle: {
                type: 'string',
                selector: 'h2',
                default: ''
            },
            stylingOption: {
                type: 'string',
                default: 'featured-posts-header-default'
            },
            excerptToggle: {
                type: 'boolean',
                default: false
            },
            selectedCount: {
                type: 'number',
                default: 0
            },
            gridSize: {
                type: 'number',
                default: ''
            },
            gridClass: {
                type: 'string',
                default: ''
            },
            selectedPosts: {
                type: 'array',
                default: [
                    { postId: null, postImage: null },
                    { postId: null, postImage: null },
                    { postId: null, postImage: null },
                    { postId: null, postImage: null },

                ]
            },
            preview: {
                type: Boolean,
                default: false
            }
        },
        /**
         * @see ./edit.js
         */
        edit: FeaturedPostsEdit,

        /**
         * @see ./save.js
         */
        save: FeaturedPostsSave,
        example: {
            attributes: {
                preview: true
            }
        }
    });

}

export default register;