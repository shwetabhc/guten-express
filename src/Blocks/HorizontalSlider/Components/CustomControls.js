import { __ } from '@wordpress/i18n';
import { useEffect } from 'react';
import { InspectorControls, MediaUpload } from '@wordpress/block-editor';
import { Button, PanelBody, TextControl, ToggleControl } from '@wordpress/components';
import verticalScrollPreview from '../assets/verticalScrollPreview.gif';
import slickDotsPreview from '../assets/slickDotsPreview.gif';
import slickArrowPreview from '../assets/slickArrowPreview.gif';
import finiteScrollPreview from '../assets/finiteScrollPreview.gif';
import adaptiveHeightPreview from '../assets/adaptiveHeightPreview.gif'


export default function ({ attributes, setAttributes, clientId, enableSlider, setEnableSlider }) {
    const {
        adaptiveHeight,
        autoplay,
        autoplaySpeed,
        arrows,
        centerMode,
        dots,
        infinite,
        rows,
        slidesToShow,
        slidesToScroll,
        vertical,
        customArrows,
        prevArrow,
        nextArrow
    } = attributes;
    useEffect(() => {
    }, [attributes])
    return (
        <InspectorControls title={__("Horizontal Slider", "guten-express")}>
            <PanelBody title={__("Horizontal Slider", "guten-express")} >
                <ToggleControl label={__("Preview Mode", "guten-express")} checked={enableSlider} onChange={(e) => {
                    setEnableSlider(() => {

                        return (!enableSlider)
                    });
                    setTimeout(() => {
                        if (!enableSlider == true) {
                            jQuery(`#${clientId}>.block-editor-inner-blocks>.block-editor-block-list__layout`).slick({
                                autoplay: autoplay,
                                autoplaySpeed: autoplaySpeed,
                                adaptiveHeight: adaptiveHeight,
                                centerPadding: "100px",
                                arrows: arrows,
                                centerMode: centerMode,
                                dots: dots,
                                infinite: infinite,
                                vertical: vertical,
                                rows: rows,
                                slidesToShow: slidesToShow,
                                slidesToScroll: slidesToScroll,
                                prevArrow: customArrows && prevArrow
                                    ? `<img class="slick-arrows slick-arrows-prev" src="${prevArrow}"/>`
                                    : '<button type="button" class="slick-prev">Previous</button>',
                                nextArrow: customArrows && nextArrow
                                    ? `<img class="slick-arrows slick-arrows-next" src="${nextArrow}"/>`
                                    : '<button type="button" class="slick-next">Next</button>'

                            });
                        }
                    }, 100)
                    if (!enableSlider == false) {
                        jQuery(`.${clientId}>.block-editor-inner-blocks>.block-editor-block-list__layout`).slick("unslick");
                    }



                }} />
                <ToggleControl

                    disabled={enableSlider}
                    label={__("Adaptive Height", "guten-express")}
                    checked={adaptiveHeight}
                    onChange={(e) => {
                        setAttributes({ adaptiveHeight: !adaptiveHeight });
                    }}
                />
                <img src={wpConstants.pluginUrl + adaptiveHeightPreview} style={{ marginBottom: 20 }} />
                <ToggleControl
                    disabled={enableSlider}
                    label={__("Auto Play", "guten-express")}
                    value={autoplay}
                    checked={autoplay}
                    onChange={(e) => {
                        setAttributes({ autoplay: !autoplay })
                    }}
                />

                <TextControl
                    disabled={enableSlider || !autoplay}
                    type="number"
                    label={__("Auto Play Speed", "guten-express")}
                    value={parseInt(autoplaySpeed)}
                    onChange={(newValue) => {
                        setAttributes({ autoplaySpeed: parseInt(newValue) });
                    }}

                />
                <ToggleControl
                    disabled={enableSlider}
                    label={__("Arrows", "guten-express")}
                    value={arrows}
                    checked={arrows}
                    onChange={(e) => {
                        setAttributes({ arrows: !arrows })
                    }}
                />
                <img src={wpConstants.pluginUrl + slickArrowPreview} style={{ marginBottom: 20 }} />

                {arrows ? <ToggleControl
                    disabled={enableSlider}
                    label={__("Custom Arrows", "guten-express")}
                    value={customArrows}
                    checked={customArrows}
                    onChange={(e) => {
                        setAttributes({ customArrows: !customArrows })
                    }}
                /> : null}
                {customArrows
                    ? (prevArrow
                        ? <div><label>Prev Arrow</label><img class="slick-arrows slick-arrows-prev" src={prevArrow} /></div>
                        : <div><label>Prev Arrow</label></div>
                    )
                    : null}
                {customArrows ?

                    <MediaUpload
                        title={__("Preview Arrow", "guten-express")}
                        onSelect={(imageData) => {
                            setAttributes({ prevArrow: imageData.url });
                        }}
                        render={({ open }) => {
                            return (
                                <Button
                                    disabled={enableSlider}
                                    style={{ marginRight: '10%', marginBottom: '10%' }}
                                    isSecondary={true}
                                    onClick={open}
                                >Upload</Button>)
                        }}

                    /> : null}
                {customArrows ? <Button disabled={enableSlider} isSecondary={true} onClick={() => {
                    setAttributes({ prevArrow: '' });

                }}>Remove</Button> : null}
                {customArrows
                    ? (nextArrow
                        ? <div><label>Next Arrow</label><img class="slick-arrows slick-arrows-next" src={nextArrow} /></div>
                        : <div><label>Next Arrow</label></div>
                    )
                    : null}
                {customArrows ? <MediaUpload

                    title={__("Next Arrow", "guten-express")}
                    onSelect={(imageData) => {
                        console.log(imageData.url);
                        console.log(imageData.id);
                        setAttributes({ nextArrow: imageData.url });
                    }}
                    render={({ open }) => {
                        return (
                            <Button
                                disabled={enableSlider}
                                style={{ marginRight: '10%', marginBottom: '10%' }}
                                isSecondary={true}
                                onClick={open}
                            >Upload</Button>)
                    }}

                /> : null}

                {customArrows ? <Button disabled={enableSlider} isSecondary={true} onClick={() => {
                    setAttributes({ nextArrow: '' });

                }}>Remove</Button> : null}


                <ToggleControl
                    disabled={enableSlider}
                    label={__("Center Mode", "guten-express")}
                    value={centerMode}
                    checked={centerMode}
                    onChange={(e) => {
                        setAttributes({ centerMode: !centerMode })
                    }}
                />
                <ToggleControl
                    disabled={enableSlider}
                    label={__("Bottom Dots", "guten-express")}
                    value={dots}
                    checked={dots}
                    onChange={(e) => {
                        setAttributes({ dots: !dots })
                    }}
                />
                <img src={wpConstants.pluginUrl + slickDotsPreview} style={{ marginBottom: 20 }} />
                <ToggleControl
                    disabled={enableSlider}
                    label={__("Rewind Scroll", "guten-express")}
                    value={!infinite}
                    checked={!infinite}
                    onChange={(e) => {
                        setAttributes({ infinite: !infinite })
                    }}
                />
                <img src={wpConstants.pluginUrl + finiteScrollPreview} style={{ marginBottom: 20 }} />
                <ToggleControl
                    disabled={enableSlider}
                    label={__("Vertical", "guten-express")}
                    value={vertical}
                    checked={vertical}
                    onChange={(e) => {
                        setAttributes({ vertical: !vertical })
                    }}
                />
                <img src={wpConstants.pluginUrl + verticalScrollPreview} style={{ marginBottom: 20 }} />
                <TextControl

                    disabled={enableSlider}
                    type="number"
                    label={__("Number of Rows", "guten-express")}
                    value={parseInt(rows)}
                    onChange={(newValue) => {
                        setAttributes({ rows: parseInt(newValue) });
                    }}

                />
                <TextControl

                    disabled={enableSlider}
                    type="number"
                    label={__("Slides Per Page", "guten-express")}
                    value={parseInt(slidesToShow)}
                    onChange={(newValue) => {
                        setAttributes({ slidesToShow: parseInt(newValue) });
                    }}

                />
                <TextControl

                    disabled={enableSlider}
                    type="number"
                    label={__("Slides To Scroll", "guten-express")}
                    value={slidesToScroll}
                    onChange={(newValue) => {
                        setAttributes({ slidesToScroll: parseInt(newValue) });
                    }}

                />


            </PanelBody>
        </InspectorControls>
    )
}

