import { __ } from '@wordpress/i18n';
import { useEffect, useState } from 'react';
import { useBlockProps, InnerBlocks, BlockControls } from '@wordpress/block-editor';
import { Button, ToggleControl } from '@wordpress/components';
import './editor.scss';
import CustomControls from './Components/CustomControls';
import horizontalScrollPreview from './assets/horizontalScrollPreview.gif';

export default function (props) {
    const { attributes, setAttributes, clientId } = props;
    const { adaptiveHeight, carouselId,
        autoplay,
        autoplaySpeed,
        arrows,
        centerMode,
        dots,
        infinite,
        rows,
        slidesToShow,
        slidesToScroll,
        nextArrow,
        prevArrow,
        customArrows,
        preview,
        vertical } = attributes;
    const [enableSlider, setEnableSlider] = useState(false);

    useEffect(() => {
        setAttributes({ carouselId: clientId })

    }, [props])


    if (preview === true) {
        return (
            <>
                <img src={wpConstants.pluginUrl + horizontalScrollPreview} />
            </>
        )
    }
    return (
        <div {...useBlockProps()}>

            <CustomControls attributes={attributes} setAttributes={setAttributes} clientId={clientId} enableSlider={enableSlider} setEnableSlider={setEnableSlider} />
            <BlockControls>
                <Button style={{ height: "100%" }} >
                    <ToggleControl label="Preview" checked={enableSlider}
                        onChange={(e) => {
                            setEnableSlider(() => {

                                return (!enableSlider)
                            });
                            setTimeout(() => {
                                if (!enableSlider == true) {
                                    jQuery(`#${clientId}>.block-editor-inner-blocks>.block-editor-block-list__layout`).slick({
                                        autoplay: autoplay,
                                        autoplaySpeed: autoplaySpeed,
                                        adaptiveHeight: adaptiveHeight,
                                        centerPadding: "100px",
                                        arrows: arrows,
                                        centerMode: centerMode,
                                        dots: dots,
                                        infinite: infinite,
                                        vertical: vertical,
                                        rows: rows,
                                        slidesToShow: slidesToShow,
                                        slidesToScroll: slidesToScroll,
                                        prevArrow: customArrows && prevArrow
                                            ? `<img class="slick-arrows slick-arrows-prev" src="${prevArrow}"/>`
                                            : '<button type="button" class="slick-prev">Previous</button>',
                                        nextArrow: customArrows && nextArrow
                                            ? `<img class="slick-arrows slick-arrows-next" src="${nextArrow}"/>`
                                            : '<button type="button" class="slick-next">Next</button>'

                                    });
                                }
                            }, 10)
                            if (!enableSlider == false) {
                                jQuery(`#${clientId}>.block-editor-inner-blocks>.block-editor-block-list__layout`).slick("unslick");
                            }
                        }} />
                </Button>

            </BlockControls>

            <div id={clientId} className={"horizontal-scroll-wrapper " + clientId} >
                <InnerBlocks templateLock={enableSlider ? "all" : false} />
            </div>
        </div>
    )
}