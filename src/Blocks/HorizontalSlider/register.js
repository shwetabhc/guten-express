import { registerBlockType } from '@wordpress/blocks';

import './style.scss';
import HorizontalSliderEdit from './edit';
import HorizontalSliderSave from './save';


const register = () => {

    registerBlockType('guten-express/horizontal-slider', {
        attributes: {
            preview: {
                type: 'boolean',
                default: false,
            },
            adaptiveHeight: {
                type: 'boolean',
                default: false
            },
            carouselId: {
                type: 'string',
                default: ''
            },
            autoplay: {
                type: 'boolean',
                default: true
            },
            autoplaySpeed: {
                type: 'number',
                default: 3000
            },
            arrows: {
                type: 'boolean',
                default: true
            },
            customArrows: {
                type: 'boolean',
                default: false
            },
            prevArrow: {
                type: 'string',
                default: ''
            },
            nextArrow: {
                type: 'string',
                default: ''
            },
            centerMode: {
                type: 'boolean',
                default: false
            },
            dots: {
                type: 'boolean',
                default: false
            },
            infinite: {
                type: 'boolean',
                default: true
            },
            rows: {
                type: 'number',
                default: 1
            },
            slidesToShow: {
                type: 'number',
                default: 1
            },
            slidesToScroll: {
                type: 'number',
                default: 1
            },
            vertical: {
                type: 'boolean',
                default: false
            }

        },
        edit: HorizontalSliderEdit,
        save: HorizontalSliderSave,
        example: {
            attributes: {
                preview: true
            }
        }
    });

}

export default register;