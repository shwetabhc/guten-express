import { __ } from '@wordpress/i18n';
import { useBlockProps, InnerBlocks } from '@wordpress/block-editor'
import './style.scss';
export default function ({ attributes }) {

    return (
        <div {...useBlockProps.save()} >
            <div id={attributes.carouselId} className={`${attributes.carouselId} horizontal-scroll-wrapper-save`}>
                <InnerBlocks.Content />
            </div>
        </div>
    )
}