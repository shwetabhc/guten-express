import { ColorPalette, PanelBody, RangeControl, Button, ToggleControl, FontSizePicker, SelectControl } from '@wordpress/components';
import { MediaUpload } from '@wordpress/block-editor';
export default function ({ attributes, setAttributes }) {
    const { cardFrontColor, cardBackColor, toggleFlip, cardBorderRadius, showBack, verticalFlip, richTextFontSize, cardWidth, cardAlignment } = attributes;
    const colors = [
        { name: 'red', color: '#f00' },
        { name: 'white', color: '#fff' },
        { name: 'blue', color: '#00f' },
    ];
    const cardAlignments = [
        {
            label: 'Left',
            value: 'flex-start'
        },
        {
            label: 'Center',
            value: 'center'
        },
        {
            label: 'Right',
            value: 'flex-end'
        }
    ]
    const fontSizes = [
        {
            name: 'Small',
            slug: 'small',
            size: 12,
        },
        {
            name: 'Normal',
            slug: 'normal',
            size: 16,
        },
        {
            name: 'Big',
            slug: 'big',
            size: 26,
        }
    ];
    return (
        <PanelBody title={"Customize Flip Card"}>
            <ToggleControl label='Enable Flip' checked={toggleFlip} onChange={(e) => {
                setAttributes({ toggleFlip: !toggleFlip });
            }} />
            <ToggleControl label='Show Back' checked={showBack} onChange={(e) => {
                setAttributes({ showBack: !showBack });
            }} />
            <ToggleControl label='Vertical Flip' checked={verticalFlip} onChange={(e) => {
                setAttributes({ verticalFlip: !verticalFlip });
            }} />

            <RangeControl
                label={"Set Card Width"}
                value={cardWidth}
                onChange={(value) => setAttributes({ cardWidth: `${value}%` })}
                min={30}
                max={100}
            />

            <SelectControl
                label={"Card Alignment"}
                value={cardAlignment}
                options={cardAlignments}
                onChange={(newAlignment) => setAttributes({ cardAlignment: newAlignment })}
            />



            <RangeControl
                label={"Set Border Radius"}
                value={cardBorderRadius}
                onChange={(value) => setAttributes({ cardBorderRadius: value })}
                min={0}
                max={200}
            />

            <FontSizePicker
                fontSizes={fontSizes}
                value={richTextFontSize}
                onChange={(size) => {
                    setAttributes({ richTextFontSize: size })
                }}
                fallbackFontSize={16}
                withSlider
            />

            <p><strong>Upload Front Image:</strong></p>
            <MediaUpload
                title={"Upload Front Image"}
                onSelect={(imageData) => {
                    console.log(imageData.url);
                    console.log(imageData.id);
                    setAttributes({ cardFrontImageUrl: imageData.url, cardFrontImageId: imageData.id });
                }}
                render={({ open }) => {
                    return (
                        <Button
                            style={{ marginRight: '10%' }}
                            isSecondary={true}
                            onClick={open}
                        >Upload</Button>)
                }}

            />
            <Button isSecondary={true} onClick={() => {
                setAttributes({ cardFrontImageUrl: '', cardFrontImageId: 0 });

            }}>Remove</Button>

            <p><strong>Upload Back Image:</strong></p>
            <MediaUpload
                title={"Upload Back Image"}
                onSelect={(imageData) => {
                    console.log(imageData.url);
                    console.log(imageData.id);
                    setAttributes({ cardBackImageUrl: imageData.url, cardBackImageId: imageData.id });
                }}
                render={({ open }) => {
                    return (
                        <Button
                            style={{ marginRight: '10%' }}
                            isSecondary={true}
                            onClick={open}
                        >Upload</Button>)
                }}

            />
            <Button isSecondary={true} onClick={() => {
                setAttributes({ cardBackImageUrl: '', cardBackImageId: 0 });

            }}>Remove</Button>



            <p><strong>Flip Card Front Color:</strong></p>
            <ColorPalette
                colors={colors}
                value={cardFrontColor}
                onChange={(color) => {
                    setAttributes({ cardFrontColor: color });

                }}
            />
            <p><strong>Flip Card Back Color:</strong></p>
            <ColorPalette
                colors={colors}
                value={cardBackColor}
                onChange={(color) => {
                    setAttributes({ cardBackColor: color });
                }}
            />

        </PanelBody>
    )
}