import { __ } from '@wordpress/i18n';
import { useBlockProps, RichText, InspectorControls, BlockControls, InnerBlocks, AlignmentToolbar } from '@wordpress/block-editor';
import CustomControls from './Components/CustomControls';
import flipCardPreview from './assets/FlipCardPreview.gif';
import './editor.scss';

export default function ({ attributes, setAttributes }) {
    const {
        verticalFlip,
        showBack,
        cardWidth,
        cardAlignment,
        cardFrontColor,
        cardBackColor,
        toggleFlip,
        cardBorderRadius,
        cardFrontImageUrl,
        cardBackImageUrl,
        richTextValue,
        richTextFontSize,
        richTextAlignment,
        preview
    } = attributes;

    if (preview === true) {
        return (
            <>
                <img src={wpConstants.pluginUrl + flipCardPreview} />
            </>
        )
    }

    return (
        <div {...useBlockProps()}>
            <InspectorControls>
                <CustomControls attributes={attributes} setAttributes={setAttributes} />
            </InspectorControls>
            <div className="box-alignment" style={{ display: 'flex', justifyContent: cardAlignment }}>
                <div className={"box-item"} style={{ width: cardWidth }}>
                    <div className={toggleFlip ? verticalFlip ? "flip-box-vertical" : "flip-box" : showBack ? "no-flip-box show-back" : "no-flip-box"}>
                        <div
                            className="flip-box-front"
                            style={{
                                backgroundColor: cardFrontColor,
                                borderRadius: cardBorderRadius,
                                backgroundImage: `url(${cardFrontImageUrl})`
                            }}>
                            <div className="inner">
                                <InnerBlocks />
                            </div>
                        </div>
                        <div
                            className="flip-box-back"
                            style={{
                                backgroundColor: cardBackColor,
                                borderRadius: cardBorderRadius,
                                backgroundImage: `url(${cardBackImageUrl})`,
                                height: '100%'
                            }}>

                            <div className="inner">
                                <BlockControls>
                                    {true && <AlignmentToolbar onClick={() => setAttributes({ richTextSelected: true })} value={richTextAlignment} onChange={(alignment) => setAttributes({ richTextAlignment: alignment })} />}
                                </BlockControls>
                                <RichText onSelect={() => setAttributes({ richTextSelected: true })} onBlur={() => setAttributes({ richTextSelected: false })} style={{ fontSize: richTextFontSize, textAlign: richTextAlignment }} placeholder={__("Enter some Text...", "guten-express")} tagName='p' value={richTextValue} onChange={(value) => setAttributes({ richTextValue: value })} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}