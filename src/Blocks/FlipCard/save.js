import { __ } from '@wordpress/i18n';
import { useBlockProps, RichText, InnerBlocks } from '@wordpress/block-editor';
import './style.scss';
export default function ({ attributes }) {
    const {
        cardFrontColor,
        cardWidth,
        cardAlignment,
        cardBackColor,
        cardBorderRadius,
        cardFrontImageUrl,
        verticalFlip,
        richTextValue,
        richTextFontSize,
        richTextAlignment,
        cardBackImageUrl } = attributes;

    return (
        <div {...useBlockProps.save()} >
            <div className="box-alignment" style={{ display: 'flex', justifyContent: cardAlignment }}>
                <div className={"box-item"} style={{ width: cardWidth }}>
                    <div className={verticalFlip ? "flip-box-vertical" : "flip-box"}>
                        <div
                            className="flip-box-front"
                            style={{
                                backgroundColor: cardFrontColor,
                                borderRadius: cardBorderRadius,
                                backgroundImage: `url(${cardFrontImageUrl})`
                            }}>
                            <div className="inner">
                                <InnerBlocks.Content />
                            </div>
                        </div>
                        <div
                            className="flip-box-back"
                            style={{
                                backgroundColor: cardBackColor,
                                borderRadius: cardBorderRadius,
                                backgroundImage: `url(${cardBackImageUrl})`,
                                height: '100%'
                            }}>
                            <div className="inner">
                                <RichText.Content style={{ fontSize: richTextFontSize, textAlign: richTextAlignment }} tagName="p" value={richTextValue} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
}