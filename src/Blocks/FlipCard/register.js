import { registerBlockType } from '@wordpress/blocks';
import './style.scss';
import FlipCardEdit from './edit';
import FlipCardSave from './save';

const register = () => {

    registerBlockType('guten-express/flip-card', {
        attributes: {
            preview: {
                type: 'boolean',
                default: false
            },
            cardWidth: {
                type: 'string',
                default: '100%'
            },
            cardAlignment: {
                type: 'string',
                default: 'center'
            },
            richTextSelected: {
                type: 'boolean',
                default: false
            },
            richTextValue: {
                type: 'string',
                selector: 'p',
                source: 'html'
            },
            richTextAlignment: {
                type: 'string',
                default: 'none'
            },
            richTextFontSize: {
                type: 'number',
                default: 16
            },
            showBack: {
                type: 'boolean',
                default: false
            },
            verticalFlip: {
                type: 'boolean',
                default: false
            },

            toggleFlip: {
                type: 'boolean',
                default: false
            },
            cardBorderRadius: {
                type: 'number',
                default: 10
            },
            cardFrontImageUrl: {
                type: 'string',
                default: ""
            },
            cardFrontImageId: {
                type: 'number',
                default: 0
            },
            cardBackImageUrl: {
                type: 'string',
                default: ""
            },
            cardBackImageId: {
                type: 'number',
                default: 0
            },
            cardFrontColor: {
                type: 'string',
                default: "#a7a7a7"
            },
            cardBackColor: {
                type: 'string',
                default: "#a7a7a7"
            }
        },
        /**
         * @see ./edit.js
         */
        edit: FlipCardEdit,

        /**
         * @see ./save.js
         */
        save: FlipCardSave,
        example: {
            attributes: {
                preview: true
            }
        }
    });

}

export default register;