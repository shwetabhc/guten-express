import registerFeaturedPosts from './Blocks/FeaturedPosts/register';
import registerFlipCard from './Blocks/FlipCard/register';
import registerHorizontalSlider from './Blocks/HorizontalSlider/register';

// Registering the Corresponding Blocks in JS
registerFeaturedPosts();
registerFlipCard();
registerHorizontalSlider();

