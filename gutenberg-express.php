<?php

/**
 * Plugin Name:       Guten Express
 * Description:       WordPress plugin for Gutenberg editor. This plugin adds new feature rich blocks in WordPress Gutenberg editor.
 * Requires at least: 5.7
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            greyhatch
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       guten-express
 *
 * @package           GutenExpress
 */

defined('ABSPATH') or die("don't sneak around");

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
	
	require_once dirname(__FILE__) . '/vendor/autoload.php';
}
function activate_guten_express()
{
	Gutenexpressinc\Base\Activate::activate();
}

function deactivate_guten_express()
{
	Gutenexpressinc\Base\Deactivate::deactivate();
}

register_activation_hook(__FILE__, 'activate_guten_express');
register_deactivation_hook(__FILE__, 'deactivate_guten_express');

if (class_exists('Gutenexpressinc\\Init')) {
	Gutenexpressinc\Init::register_services();
}
