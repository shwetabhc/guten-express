<?php

/**
 * @package GutenExpress
 */

namespace Gutenexpressinc\Base;

class Enqueue
{

    function register()
    {
        add_action('init', array($this, 'enqueue'));
    }
    function enqueue()
    {

        wp_enqueue_style('slick-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css', array());
        wp_enqueue_style('slick-theme', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css", array());
        wp_register_script('slick-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js', array('jquery'));
        wp_enqueue_script('slick-carousel');

        wp_register_script(
            'guten-express-script',
            plugins_url('guten-express/build/index.js'),
            array(
                'wp-i18n',
                'wp-edit-post',
                'wp-element',
                'wp-editor',
                'wp-components',
                'wp-data',
                'wp-plugins',
                'wp-edit-post',
                'wp-api',
                'jquery'
            )
        );
        wp_register_style('guten-express-style-editor', plugins_url('guten-express/build/index.css'), array());
        wp_register_style('guten-express-style', plugins_url('guten-express/build/style-index.css'), array());
        $thumbnailData = [];
        foreach (get_posts() as $postData) {
            if (get_post_thumbnail_id($postData))

                $thumbnailData[] = [
                    'id' => get_post_thumbnail_id($postData),
                    'source_url' => get_the_post_thumbnail_url($postData),
                ];
        }
        wp_localize_script(
            'guten-express-script',
            'wpConstants',
            [
                'allThumbnails' => $thumbnailData,
                'siteUrl' => get_site_url(),
                'pluginUrl' => plugins_url('guten-express/build/'),
                'previewData' => [
                    [
                        'postID' => get_posts()[0]->ID,
                        'FeaturedID' => get_post_thumbnail_id(get_posts()[0]->ID)
                    ],
                    [
                        'postID' => get_posts()[3]->ID,
                        'FeaturedID' => get_post_thumbnail_id(get_posts()[3]->ID)
                    ]
                ]
            ]
        );
    }
}
