<?php

/**
 * @package GutenExpress
 */

namespace Gutenexpressinc\Base;

class Activate
{
    public static function activate()
    {
        flush_rewrite_rules();
    }
}
