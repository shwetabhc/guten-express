<?php

/**
 * @package GutenExpress
 */

namespace Gutenexpressinc\Blocks;

class HorizontalSlider
{
    function register()
    {
        add_action('init', array($this, 'register_horizontal_slider'));
    }

    function register_horizontal_slider()
    {
        register_block_type('guten-express/horizontal-slider', array(
            'editor_script' => 'guten-express-script',
            "textdomain" => "guten-express",
            'api_version' => 2,
            "title" => __("Horizontal Slider", "guten-express"),
            "category" => "widgets",
            "icon" => "leftright",
            "description" => __("Customizable Carousel", "guten-express"),
            'supports' => array(
                'html' => false
            ),
            'editor_style' => 'guten-express-style-editor',
            'style' => 'guten-express-style',
            'attributes' => array(
                'preview' => [
                    'type' => 'boolean',
                    'default' => false
                ],
                'adaptiveHeight' => [
                    'type' => 'boolean',
                    'default' => false
                ],
                'carouselId' => [
                    'type' => 'string',
                    'default' => ''
                ],
                'autoplay' => [
                    'type' => 'boolean',
                    'default' => true
                ],
                'autoplaySpeed' => [
                    'type' => 'number',
                    'default' => 3000
                ],
                'arrows' => [
                    'type' => 'boolean',
                    'default' => true
                ],
                'prevArrow' => [
                    'type' => 'string',
                    'default' => ''
                ],
                'nextArrow' => [
                    'type' => 'string',
                    'default' => ''
                ],
                'centerMode' => [
                    'type' => 'boolean',
                    'default' => false
                ],
                'dots' => [
                    'type' => 'boolean',
                    'default' => false
                ],
                'infinite' => [
                    'type' => 'boolean',
                    'default' => true
                ],
                'rows' => [
                    'type' => 'number',
                    'default' => 1
                ],
                'slidesToShow' => [
                    'type' => 'number',
                    'default' => 1
                ],
                'slidesToScroll' => [
                    'type' => 'number',
                    'default' => 1
                ],
                'vertical' => [
                    'type' => 'boolean',
                    'default' => false
                ]
            ),
            'render_callback' => [$this, 'render_guten_express_horizontal_slider']
        ));
    }

    function render_guten_express_horizontal_slider($attributes, $content)
    {
        // var_dump($attributes);
        $carouselId = $attributes['carouselId'];
        $adaptiveHeight = $attributes['adaptiveHeight'] == true ? 'true' : 'false';
        $autoplay = $attributes['autoplay'] == true ? 'true' : 'false';
        $arrows = $attributes['arrows'] == true ? 'true' : 'false';
        $centerMode = $attributes['centerMode'] == true ? 'true' : 'false';
        $dots = $attributes['dots'] == true ? 'true' : 'false';
        $infinite = $attributes['infinite'] == true ? 'true' : 'false';
        $vertical = $attributes['vertical'] == true ? 'true' : 'false';
        $autoplaySpeed = $attributes['autoplaySpeed'];
        $rows = $attributes['rows'];
        $slidesToShow = $attributes['slidesToShow'];
        $slidesToScroll = $attributes['slidesToScroll'];
        $prevArrow = $attributes['prevArrow'] ? '<img class="' . 'slick-arrows slick-arrows-prev"' . 'src="' . $attributes['prevArrow'] . '" />' : '<button type="button" class="slick-prev">Previous</button>';
        $nextArrow = $attributes['nextArrow'] ? '<img class="' . 'slick-arrows slick-arrows-next"' . 'src="' . $attributes['nextArrow'] . '" />' : '<button type="button" class="slick-next">Next</button>';




        $script = "<script>
            jQuery(document).ready(function() {
                jQuery('#{$carouselId}').slick({
                    autoplay: {$autoplay},
                    autoplaySpeed: {$autoplaySpeed},
                    arrows: {$arrows},
                    centerPadding: '1000px',
                    adaptiveHeight: {$adaptiveHeight},
                    centerMode: {$centerMode},
                    dots: {$dots},
                    infinite: {$infinite},
                    vertical: {$vertical},
                    rows: {$rows},
                    slidesToShow: {$slidesToShow},
                    slidesToScroll: {$slidesToScroll},
                    prevArrow: '{$prevArrow}',
                    nextArrow: '{$nextArrow}'
                })
            });
        </script>";

        $content .= $script;
        return $content;
    }
}
