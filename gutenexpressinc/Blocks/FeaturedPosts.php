<?php

/**
 * @package GutenExpress
 */

namespace Gutenexpressinc\Blocks;

class FeaturedPosts
{
    function register()
    {
        add_action('init', array($this, 'register_featured_posts'));
    }

    function register_featured_posts()
    {
        register_block_type('guten-express/featured-posts', array(
            'editor_script' => 'guten-express-script',
            "textdomain" => "guten-express",
            'api_version' => 2,
            "title" => __("Featured Posts", "guten-express"),
            "category" => "widgets",
            "icon" => "grid-view",
            "description" => __("Display Your favourite Posts with just a few clicks", "guten-express"),
            'supports' => array(
                'html' => false
            ),
            'editor_style' => 'guten-express-style-editor',
            'style' => 'guten-express-style',
            'render_callback' => [$this, 'render_guten_express_featured_posts'],
            'attributes' => array(
                'blockTitle' => [
                    'type' => 'string',
                    'selector' => 'h2',
                    'default' => ''
                ],
                'stylingOption' => [
                    'type' => 'string',
                    'default' => 'featured-posts-header-default'
                ],
                'excerptToggle' => [
                    'type' => 'boolean',
                    'default' => false
                ],
                'selectedCount' => [
                    'type' => 'number',
                    'default' => 0
                ],
                'gridSize' => [
                    'type' => 'number',
                    'default' => ''
                ],
                'gridClass' => [
                    'type' => 'string',
                    'default' => ''
                ],
                'preview' => [
                    'type' => 'boolean',
                    'default' => false
                ],
                'selectedPosts' => [
                    'type' => 'array',
                    'default' => [
                        ['postId' => null, 'postImage' => null],
                        ['postId' => null, 'postImage' => null],
                        ['postId' => null, 'postImage' => null],
                        ['postId' => null, 'postImage' => null],

                    ]
                ]
            ),
        ));
    }

    function render_guten_express_featured_posts($attributes)
    {
        $postCount = 0;
        $blockElement = "<div class='wp-block-guten-express-featured-posts'>";
        $blockTitleElement = "<h2>{$attributes['blockTitle']}</h2>";
        $blockElement .= $blockTitleElement;

        $gridClass = isset($attributes['gridClass']) ? $attributes['gridClass'] : '';

        $postElements = "<div class='featured-posts-container {$gridClass}'>";
        if (isset($attributes['selectedPosts'])) {
            foreach ($attributes['selectedPosts'] as $post_data) {
                $postCount++;

                if ($postCount <= $attributes['gridSize']) {
                    $postContainer = "<div class='featured-posts-grid'>";
                    $postThumbnailUrl = get_the_post_thumbnail_url($post_data['postId']);
                    $styleClass = $postThumbnailUrl != false ? (isset($attributes['stylingOption']) ? $attributes['stylingOption'] : '') : '';

                    $postHeaderContainer = "<div class='{$styleClass}'><div class='overlay'></div>";

                    $postTitleContent = get_the_title($post_data["postId"]);
                    $postPermalink = get_permalink($post_data["postId"]);
                    $postTitleElement = "<h3><a href='{$postPermalink}'>{$postTitleContent}</a></h3>";

                    $postThumbnailElement = $postThumbnailUrl != false ? "<img src='{$postThumbnailUrl}' >" : '';

                    $postHeaderContainer = $postHeaderContainer . $postTitleElement . $postThumbnailElement . "</div>";

                    $postExcerptElement = '';
                    if ($attributes['excerptToggle']) {
                        $postExcerptContent = get_the_excerpt($post_data['postId']);
                        $postExcerptElement .= "<div>{$postExcerptContent}</div>";
                    }
                    $postContainer .= $postHeaderContainer;
                    $postContainer .= $postExcerptElement;
                    $postContainer .= "</div>";
                    $postElements .= $postContainer;
                }
            }
        }
        $postElements .= "</div>";
        $blockElement .= $postElements;
        $blockElement .= "</div>";
        return $blockElement;
    }
}
