<?php

/**
 * @package GutenExpress
 */

namespace Gutenexpressinc\Blocks;

class FlipCard
{
    function register()
    {
        add_action('init', array($this, 'register_flip_card'));
    }

    function register_flip_card()
    {
        register_block_type('guten-express/flip-card', array(
            'editor_script' => 'guten-express-script',
            "textdomain" => "guten-express",
            'api_version' => 2,
            "title" => __("Flip Card", "guten-express"),
            "category" => "widgets",
            "icon" => "image-flip-horizontal",
            "description" => __("Completely dynamic and customizable Flip card with quality animations", "guten-express"),
            'supports' => array(
                'html' => false
            ),
            'editor_style' => 'guten-express-style-editor',
            'style' => 'guten-express-style',
            'attributes' => array(),
        ));
    }
}
