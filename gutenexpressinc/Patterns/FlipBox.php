<?php

/**
 * @package GutenExpress
 */

namespace Gutenexpressinc\Patterns;

class FlipBox
{
    function register()
    {
        add_action('init', array($this, 'register_flip_box_pattern'));
        add_action('init', array($this, 'register_flip_box_pattern_category'));
    }

    function register_flip_box_pattern_category()
    {
        register_block_pattern_category(
            'flipcard',
            array('label' => __('Flip Card Patterns', 'guten-express'))
        );
    }

    function register_flip_box_pattern()
    {
        register_block_pattern(
            'guten-express/flip-box-pattern-one',
            array(
                'title'       => __('Flip Card Pattern One', 'guten-express'),
                'description' => _x('Two FlipCards side by side', 'Block pattern description', 'guten-express'),
                'categories' => ['flipcard'],
                'content'     => '<!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->',
            )
        );

        register_block_pattern(
            'guten-express/flip-box-pattern-',
            array(
                'title'       => __('Flip Card Pattern Two', 'guten-express'),
                'description' => _x('Three FlipCards side by side', 'Block pattern description', 'guten-express'),
                'categories' => ['flipcard'],
                'content'     => '<!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"verticalFlip":true,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box-vertical"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->',
            )
        );

        register_block_pattern(
            'guten-express/flip-box-pattern-three',
            array(
                'title'       => __('FlipBox Pattern Three', 'guten-express'),
                'description' => _x('A Prebuilt layout for FlipCard blocks', 'Block pattern description', 'guten-express'),
                'categories' => ['flipcard'],
                'content'     => '<!-- wp:group -->
                <div class="wp-block-group"><!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"cardFrontColor":"#fff","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:heading {"textColor":"white"} -->
                <h2 class="has-white-color has-text-color">Heading</h2>
                <!-- /wp:heading -->
                
                <!-- wp:gallery {"linkTo":"none"} -->
                <figure class="wp-block-gallery columns-0 is-cropped"><ul class="blocks-gallery-grid"></ul></figure>
                <!-- /wp:gallery -->
                
                <!-- wp:paragraph -->
                <p></p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:group -->',
            )
        );

        register_block_pattern(
            'guten-express/flip-box-pattern-four',
            array(
                'title'       => __('FlipBox Pattern Four', 'guten-express'),
                'description' => _x('A Prebuilt layout for FlipCard blocks', 'Block pattern description', 'guten-express'),
                'categories' => ['flipcard'],
                'content'     => '<!-- wp:group -->
                <div class="wp-block-group"><!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":""} -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:group -->',
            )
        );

        register_block_pattern(
            'guten-express/flip-box-pattern-five',
            array(
                'title'       => __('FlipBox Pattern Five', 'guten-express'),
                'description' => _x('A Prebuilt layout for FlipCard blocks', 'Block pattern description', 'guten-express'),
                'categories' => ['flipcard'],
                'content'     => '<!-- wp:group -->
                <div class="wp-block-group"><!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column {"width":""} -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:group -->',
            )
        );

        register_block_pattern(
            'guten-express/flip-box-pattern-six',
            array(
                'title'       => __('FlipBox Pattern Six', 'guten-express'),
                'description' => _x('A Prebuilt layout for FlipCard blocks', 'Block pattern description', 'guten-express'),
                'categories' => ['flipcard'],
                'content'     => '<!-- wp:group -->
                <div class="wp-block-group"><!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"cardFrontColor":"#fff","cardBackColor":"#fff"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:heading -->
                <h2>Enter Heading</h2>
                <!-- /wp:heading -->
                
                <!-- wp:html /-->
                
                <!-- wp:paragraph {"align":"center"} -->
                <p class="has-text-align-center"> Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#fff;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none">Enter Text Here... </p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns -->
                
                <!-- wp:columns -->
                <div class="wp-block-columns"><!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"cardFrontColor":"#fff","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#fff;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:gallery {"linkTo":"none"} -->
                <figure class="wp-block-gallery columns-0 is-cropped"><ul class="blocks-gallery-grid"></ul></figure>
                <!-- /wp:gallery -->
                
                <!-- wp:paragraph -->
                <p></p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column -->
                
                <!-- wp:column -->
                <div class="wp-block-column"><!-- wp:guten-express/flip-card {"richTextFontSize":28,"toggleFlip":true,"cardFrontColor":"#000000","cardBackColor":"#000000"} -->
                <div class="wp-block-guten-express-flip-card"><div class="box-alignment" style="display:flex;justify-content:center"><div class="box-item" style="width:100%"><div class="flip-box"><div class="flip-box-front" style="background-color:#000000;border-radius:10px;background-image:url()"><div class="inner"><!-- wp:image -->
                <figure class="wp-block-image"><img alt=""/></figure>
                <!-- /wp:image -->
                
                <!-- wp:paragraph {"align":"center","textColor":"white"} -->
                <p class="has-text-align-center has-white-color has-text-color">Add Caption</p>
                <!-- /wp:paragraph --></div></div><div class="flip-box-back" style="background-color:#000000;border-radius:10px;background-image:url();height:100%"><div class="inner"><p style="font-size:28px;text-align:none"><span class="has-inline-color has-white-color">Enter Text Here...</span></p></div></div></div></div></div></div>
                <!-- /wp:guten-express/flip-card --></div>
                <!-- /wp:column --></div>
                <!-- /wp:columns --></div>
                <!-- /wp:group -->',
            )
        );
    }
}
