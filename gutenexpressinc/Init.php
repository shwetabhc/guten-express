<?php

/**
 * @package GutenExpress
 */

namespace Gutenexpressinc;

class Init
{

    public static function get_services()
    {
        return [
            Base\Enqueue::class,
            Blocks\FeaturedPosts::class,
            Blocks\FlipCard::class,
            Blocks\HorizontalSlider::class,
            Patterns\FlipBox::class,

        ];
    }

    public static function register_services()
    {
        foreach (self::get_services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {

                $service->register();
            }
        }
    }

    private static function instantiate($class)
    {
        $service = new $class();
        return $service;
    }
}
